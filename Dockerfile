FROM node:alpine

WORKDIR /app

COPY package*.json ./

#RUN ulimit -u 1024
RUN npm install express --save

COPY . .

EXPOSE 3000

ENTRYPOINT ["npm", "start"]
CMD ["node","app.js"]