# CI-CD-Pipeline
# CI-CD-Pipeline

<img src=imgs/cover.png>

### Tools: 
 - `GitLab`: To Create a public repository & Bulid  CI/CD pipeline for  simple Nodejs project.
 - `Docker`: To Build  image using the Dockerfile & Push the Docker image to a container registry in Docker Hub.
 - `AWS EC2`: To Deploy an OpenShift cluster.
 - `OpenShift`: TO Deploy the MongoDB database & Deploy the Node.js application using the Docker image from the container registry.
 - `ArgoCD`: TO Set up ArgoCD applications for the Node.js application and MongoDB database



<img src=imgs/istall.png>

## Prerequisites

- [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html) configured with appropriate permissions
- [Docker](https://docs.docker.com/engine/install/) installed and configured
- [kubectl](https://kubernetes.io/docs/tasks/tools/) installed and configured to interact with your Kubernetes cluster
- [Helm](https://helm.sh/docs/intro/install/) installed
- [GitHub_CLI](https://github.com/cli/cli) installed
- [K9s](https://k9scli.io/topics/install/) installed
- [Studio_3T](https://studio3t.com/download/) OR [MongoDB_Compass](https://www.mongodb.com/try/download/atlascli)




<img src=imgs/end.png>